<?php

namespace App\Http\Controllers;
use App\Pais;
use App\Ciudad;
use Illuminate\Http\Request;

class PaisController extends Controller
{
   
    public function states($iso2){
        $pais = Pais::where("iso2",$iso2)->first();
        if($pais){
            $states = $pais->states;
            return response()->json($states);
        }else{
            abort(403, 'Pais no encontrado');
        }
    }

    public function ciudades($iso2,$ciudad){
        $pais = Pais::where("iso2",$iso2)->first();
        if($pais){
            $ciudades = Ciudad::where('uncode',$ciudad)->where('iso2',$iso2)->get();
            return response()->json($ciudades);
        }else{
            abort(403, 'Pais no encontrado');
        }
    }

    public function ciudadSearch($ciudad){
        $ciudades = Ciudad::where('name_utf8','like',"%$ciudad%")->get();
        return response()->json($ciudades);
    }

}
