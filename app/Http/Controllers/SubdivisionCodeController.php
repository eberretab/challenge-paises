<?php

namespace App\Http\Controllers;

use App\SubdivisionCode;
use Illuminate\Http\Request;

class SubdivisionCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubdivisionCode  $subdivisionCode
     * @return \Illuminate\Http\Response
     */
    public function show(SubdivisionCode $subdivisionCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubdivisionCode  $subdivisionCode
     * @return \Illuminate\Http\Response
     */
    public function edit(SubdivisionCode $subdivisionCode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubdivisionCode  $subdivisionCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubdivisionCode $subdivisionCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubdivisionCode  $subdivisionCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubdivisionCode $subdivisionCode)
    {
        //
    }
}
