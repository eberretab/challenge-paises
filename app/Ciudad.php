<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    protected $table = 'ciudades';

    protected $fillable = ["iso2", "uncode", "name_utf8", "name_plain", "F", "use", "H", "I", "J", "K", "L", "M"];

}
