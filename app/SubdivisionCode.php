<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubdivisionCode extends Model
{
    protected $fillable = ['country_iso2_code', 'state_code', 'nombre', 'tipo'];
}
