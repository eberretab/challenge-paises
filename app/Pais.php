<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table = "paises";

    protected $fillable = ['nombre','name','nom','iso2','iso3','phone_code'];

    public function states(){
        return $this->hasMany(SubdivisionCode::class,'country_iso2_code','iso2');
    }
}
