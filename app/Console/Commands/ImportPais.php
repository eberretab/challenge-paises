<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Pais;
use App\SubdivisionCode;
use App\Ciudad;

class ImportPais extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:pais {--paisesCSV=} {--subdivisionsCSV=} {--ciudadesCSV=} {code?*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa la información de uno o más paises';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(){
        $paises =  array_map('strtoupper',$this->argument('code')); 
        
        if($paisesCSV = $this->option("paisesCSV")){
            $this->importPaises($paisesCSV,$paises);
        }
        if($subdivisionsCSV = $this->option("subdivisionsCSV")){
            $this->importSubdivisions($subdivisionsCSV,$paises);
        }
       if($ciudadesCSV = $this->option("ciudadesCSV")){
            $this->importCiudades($ciudadesCSV,$paises);
        }
        $this->info('The command was successful!');
    }

    private function importPaises($path, $paises){
        $file_handle = fopen($path, 'r');
        while (!feof($file_handle)) {
            $rows[] = fgetcsv($file_handle, 0, ',');
        }
        fclose($file_handle);

        for ($i=1; $i < count($rows) ; $i++) {
            if(is_array($rows[$i])){
                if(count($paises)==0 || in_array(strtoupper($rows[$i][3]), $paises)){
                    $this->line("Importing pais: " .$rows[$i][3]);
                    
                    $pais = Pais::where("iso2",$rows[$i][3])->first();
                    if(!$pais)
                        $pais = new Pais;

                    foreach($rows[0] as $key => $field){
                        $pais[trim($field)] = $rows[$i][$key];
                    }
                    $pais->save();
                    $this->info("Imported pais: " .$rows[$i][3]);
                }
            }
        }
    }

    private function importSubdivisions($path, $paises){
        $file_handle = fopen($path, 'r');
        while (!feof($file_handle)) {
            $rows[] = fgetcsv($file_handle, 0, ',');
        }
        fclose($file_handle);
        for ($i=1; $i < count($rows) ; $i++) { 
            if(is_array($rows[$i])){
                if(count($paises)==0 || in_array(strtoupper($rows[$i][0]), $paises)){
                    $this->line("Importing subdivision: " .$rows[$i][1]);

                    $subdivision = SubdivisionCode::where("state_code",$rows[$i][1])->first();
                    if(!$subdivision)
                        $subdivision = new SubdivisionCode;

                    $subdivision['country_iso2_code'] = isset($rows[$i][0]) ? utf8_encode($rows[$i][0])  :"";
                    $subdivision['state_code'] = isset($rows[$i][1]) ? utf8_encode($rows[$i][1])  :"";
                    $subdivision['nombre'] = isset($rows[$i][2]) ? utf8_encode($rows[$i][2])  :"";
                    $subdivision['tipo'] = isset($rows[$i][3]) ? utf8_encode($rows[$i][3])  :"";
                    $subdivision->save();
                    $this->info("Imported subdivision: " .$rows[$i][1]);
                }
            }
        }
    }

    private function importCiudades($path, $paises){
        $file_handle = fopen($path, 'r');
        while (!feof($file_handle)) {
            $rows[] = fgetcsv($file_handle, 0, ',');
        }
        fclose($file_handle);

        for ($i=1; $i < count($rows) ; $i++) { 
            if(is_array($rows[$i])){
                if(count($paises)==0 || in_array(strtoupper($rows[$i][1]), $paises)){
                    if($rows[$i][2]!=''){
                        $this->line("Importing ciudad: " .$rows[$i][3]);
                        $ciudad = Ciudad::where("uncode",$rows[$i][2])->first();
                        if(!$ciudad)
                            $ciudad = new Ciudad;
                        $ciudad['iso2'] = (isset($rows[$i][1])) ? utf8_encode($rows[$i][1]):  "";
                        $ciudad['uncode'] = (isset($rows[$i][2])) ? utf8_encode($rows[$i][2]):  "";
                        $ciudad['name_utf8'] = (isset($rows[$i][3])) ? utf8_encode($rows[$i][3]):  "";
                        $ciudad['name_plain'] = (isset($rows[$i][4])) ? utf8_encode($rows[$i][4]):  "";
                        $ciudad['F'] = (isset($rows[$i][5])) ? utf8_encode($rows[$i][5]):  "";
                        $ciudad['use'] = (isset($rows[$i][6])) ? utf8_encode($rows[$i][6]):  "";
                        $ciudad['H'] = (isset($rows[$i][7])) ? utf8_encode($rows[$i][7]):  "";
                        $ciudad['I'] = (isset($rows[$i][8])) ? utf8_encode($rows[$i][8]):  "";
                        $ciudad['J'] = (isset($rows[$i][9])) ? utf8_encode($rows[$i][9]):  "";
                        $ciudad['K'] = (isset($rows[$i][10])) ? utf8_encode($rows[$i][10]):  "";
                        $ciudad['L'] = (isset($rows[$i][11])) ? utf8_encode($rows[$i][11]):  "";
                        $ciudad['M'] = (isset($rows[$i][12]))? utf8_encode($rows[$i][12]) : "";
                        $ciudad->save();
                        $this->info("Imported ciudad: " .$rows[$i][3]);
                    }
                }
            }
        }
    }

}
