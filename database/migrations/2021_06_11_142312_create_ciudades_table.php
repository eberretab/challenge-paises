<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCiudadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ciudades', function (Blueprint $table) {
            $table->id();
            $table->string("iso2");
            $table->string("uncode");
            $table->string("name_utf8");
            $table->string("name_plain");
            $table->string("F");
            $table->string("use");
            $table->string("H");
            $table->string("I");
            $table->string("J");
            $table->string("K");
            $table->string("L");
            $table->string("M");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ciudades');
    }
}
